import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";

const FORBIDDEN_PASSWORDS = [
    "123456", "123456789", "picture1", "password", "12345678", 
    "111111","123123", "12345", "1234567890", "senha"
]

export class SignupValidators {

    public static nameValidator(control: AbstractControl): ValidationErrors | null {
        let name = control.value.trim()
        let nameParts = name.split(' ')
        
        if (name == '' || nameParts.length >= 2) {
            return null;
        }

        return {
            nameInvalid: true
        };
    }

    public static loginValidator(control: AbstractControl): ValidationErrors | null {
        // Validar que o login não tenha espaços e comece com letra ou _
        let login = control.value.toLowerCase()

        let firstLetter = login.charCodeAt(0)
        
        if (login.indexOf(' ') === -1 && 
            (login.charAt(0) === '_' || (firstLetter >= 97 && firstLetter <= 122))) {
            return null
        }        

        return { loginInvalid: true }
    }

    public static passwordValidator(control: AbstractControl): ValidationErrors | null {
        let password = control.value
        
        if (!FORBIDDEN_PASSWORDS.includes(password)) {
            return null
        }

        return { passwordInvalid : true }
    }

    public static passwordMatchValidator(form: FormGroup): ValidationErrors | null {
        let password = form.value.password
        let confirmPassword = form.value.confirmPassword

        if (password === confirmPassword) {
            return null
        }

        return { passwordsDontMatch : true }
    }

}