import { Component } from '@angular/core';
import { AbstractControlOptions, FormBuilder, Validators } from '@angular/forms';
import { SignupValidators } from './validators/signup.validators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    signupForm = this.formBuilder.group({
        name: ['',
            [ Validators.required, SignupValidators.nameValidator ] 
        ],
        email: ['', 
            [ Validators.required, Validators.email ]
        ],
        login: ['', 
            [ Validators.required, Validators.minLength(5), SignupValidators.loginValidator ] 
        ],
        password: ['', 
            [ 
                Validators.required, Validators.minLength(4), 
                Validators.maxLength(8), SignupValidators.passwordValidator 
            ] 
        ],
        confirmPassword: ['', 
            [ Validators.required ]
        ]
    }, { validators: [ SignupValidators.passwordMatchValidator ]} as AbstractControlOptions)

    constructor(private formBuilder: FormBuilder) {
        
    }

    get name() {
        return this.signupForm.controls.name
    }
    
    get email() {
        return this.signupForm.controls.email
    }

    get login() {
        return this.signupForm.controls.login
    }
    
    get password() {
        return this.signupForm.controls.password
    }
}
